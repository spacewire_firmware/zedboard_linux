do_install_append() {

sed -i 's@^#LoadModule cgid_module /usr/libexec/apache2/modules/mod_cgid.so@LoadModule cgid_module /usr/libexec/apache2/modules/mod_cgid.so@' ${D}/${sysconfdir}/${BPN}/httpd.conf

sed -i 's@^DocumentRoot "/usr/share/apache2/default-site/htdocs"@DocumentRoot "/srv/www"@' ${D}/${sysconfdir}/${BPN}/httpd.conf

sed -i 's@^<Directory "/usr/share/apache2/default-site/htdocs">@<Directory "/srv/www">@' ${D}/${sysconfdir}/${BPN}/httpd.conf

sed -i 's@^    ScriptAlias /cgi-bin/ "/usr/libexec/apache2/modules/cgi-bin/"@    ScriptAlias /cgi-bin/ "/srv/www/cgi-bin/"@' ${D}/${sysconfdir}/${BPN}/httpd.conf

sed -i 's@^<Directory "/usr/libexec/apache2/modules/cgi-bin">@<Directory "/srv/www/cgi-bin">@' ${D}/${sysconfdir}/${BPN}/httpd.conf

sed -i 's@^    DirectoryIndex index.html@    DirectoryIndex index.html index.php@' ${D}/${sysconfdir}/${BPN}/httpd.conf

sed -i 's@^User daemon@#User daemon@' ${D}/${sysconfdir}/${BPN}/httpd.conf

sed -i 's@^Group daemon@#Group daemon@' ${D}/${sysconfdir}/${BPN}/httpd.conf

}
//
// File .......... peekstring.c
// Author ........ Steve Haywood
// Version ....... 1.0
// Date .......... 29 October 2021
// Description ...
//   CGI type module to read a string from memory and return it.
// Usage ...
//   http://<web address>/cgi-bin/peekstring?base&size&offset&maxchars
// base = Base address of memory region to be used
// size = Size of memory region to be used
// offset = Offset address of string within memory region
// maxchars = Maximum length of return string
//


#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <fcntl.h>
#include <sys/mman.h>
#include <stdint.h>
#include <unistd.h>


int main()
{
  int fd;              // File descriptor for mapped memory
  char *map;           // Pointer to mapped memory
  uint32_t base;       // Start address of area to map
  uint32_t size;       // Size of area to map
  uint32_t addr;       // Address within area to access
  uint32_t length;     // Maximum length of string to return
  char *ptr;           // Pointer to character of string
  char *querystring;   // Pointer to QUERY_STRING
  char *rest;          // Pointer to next token within QUERY_STRING
  char *token;         // Pointer to extracted token
  char *endptr = NULL; // Pointer to end of string in str2int conversion
  uint32_t count = 0;  // Number of character output from string

  printf("Content-Type: text/plain;charset=us-ascii\n\n");

  querystring = getenv("QUERY_STRING");
  if (querystring)
  {
    rest = querystring;
    token = strtok_r(rest, "&", &rest);
    if (token != NULL) {
      base = strtoul(token, &endptr, 0);
      if (token != endptr) {
        token = strtok_r(rest, "&", &rest);
        if (token != NULL) {
          size = strtoul(token, &endptr, 0);
          if (token != endptr) {
            token = strtok_r(rest, "&", &rest);
            if (token != NULL) {
              addr = strtoul(token, &endptr, 0);
              if (token != endptr) {
                token = strtok_r(rest, "&", &rest);
                if (token != NULL) {
                  length = strtoul(token, &endptr, 0);
                  if (token != endptr) {
                    fd = open( "/dev/mem", O_RDWR);
                    if (fd > 0) {
                      map = mmap(NULL, size, (PROT_READ | PROT_WRITE), MAP_SHARED, fd, base);
                      if (map != MAP_FAILED) {
                        ptr = (char *)(map + addr);
                        while (*ptr != '\0' && count < length) {
                          printf("%c", *ptr);
                          ptr++;
                          count++;
                        }
                        munmap(map, size);
                      } else printf("Error: Memory to mmap");
                      close(fd);
                    } else printf("Error: Failed to open /dev/mem");
                  } else printf("Error: Invalid string length");
                } else printf("Error: Missing string length");
              } else printf("Error: Invalid string offset address");
            } else printf("Error: Missing string offset address");
          } else printf("Error: Invalid memory map size");
        } else printf("Error: Missing memory map size");
      } else printf("Error: Invalid memory map base address");
    } else printf("Error: Missing memory map base address");
  } else printf("Error: No QUERY_STRING");
}

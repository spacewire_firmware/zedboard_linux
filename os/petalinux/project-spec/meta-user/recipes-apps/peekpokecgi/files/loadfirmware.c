//
// File .......... loadfirmware.c
// Author ........ Steve Haywood
// Website ....... http://www.spacewire.co.uk
// Project ....... SpaceWire UK Tutorial
// Version ....... 1.0
// Conception .... 9 February 2024
// Standard ...... C17 (ISO/IEC 9899:2018)
// Description ...
//   Very simple CGI to load PL bitstream.
//

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <fcntl.h>
#include <string.h>

int main()
{
  char *query;
  char *firmware = "/sys/class/fpga_manager/fpga0/firmware";
  char *target = "/lib/firmware";
  int   fd;

  int sl = symlink("/media/sd-mmcblk0p1/firmware", "/lib/firmware");

  printf("Content-Type: text/plain\n\n");

  query = getenv("QUERY_STRING");
  if (query) {
    fd = open(firmware, O_WRONLY);
    if (fd >= 0) {
      write(fd, query, strlen(query));
      close(fd);
    } else printf("Error: Failed to open %s", firmware);
  } else printf("Error: No QUERY_STRING");
}

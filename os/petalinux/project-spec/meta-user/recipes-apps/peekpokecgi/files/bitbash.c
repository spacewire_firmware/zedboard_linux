//
// File .......... bitbash.c
// Author ........ Steve Haywood
// Version ....... 1.1
// Date .......... 28 February 2024
// Description ...
//   Very simple CGI application for peeking, poking and general bit bashing
// of a single 32-bit address location. Provides read back value or an error
// message to the client side application.
//
// Examples :-
//   http://192.168.2.87/cgi-bin/bitbash?peek&0x40010000 ......... Peek value @ 0x40010000
//   http://192.168.2.87/cgi-bin/bitbash?poke&0x40010000&0xF ..... Poke 0xF @ 0x40010000
//   http://192.168.2.87/cgi-bin/bitbash?set&0x40010000&0x81 ..... Set bits 0 & 7 @ 0x40010000
//   http://192.168.2.87/cgi-bin/bitbash?clear&0x40010000&0x2 .... Clear bit 1 @ 0x40010000
//   http://192.168.2.87/cgi-bin/bitbash?toggle&0x40010000&0xC ... Toggle bits 2 & 3 @ 0x40010000
//

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/mman.h>
#include <fcntl.h>

// Command Types
#define c_unknown 0
#define c_peek 1
#define c_poke 2
#define c_set 3
#define c_clear 4
#define c_toggle 5

int main()
{
  char *query;
  char *ptype;
  char *paddr;
  char *pdata;
  char *pgarbage;
  const char sep[2] = "&";
  char err = 0;
  char type;
  int fd;
  void *ptr;
  unsigned data;
  unsigned val;
  unsigned addr, page_addr, page_offset;
  unsigned page_size = sysconf(_SC_PAGESIZE);
  printf("Content-Type: text/plain;charset=us-ascii\n\n");
  query = getenv("QUERY_STRING");
  if (query) {
    if (ptype = strtok(query, sep)) {
      if (strcmp(ptype, "peek") == 0) {
        type = c_peek;
      } else if (strcmp(ptype, "poke") == 0) {
        type = c_poke;
      } else if (strcmp(ptype, "set") == 0) {
        type = c_set;
      } else if (strcmp(ptype, "clear") == 0) {
        type = c_clear;
      } else if (strcmp(ptype, "toggle") == 0) {
        type = c_toggle;
      } else {
        type = c_unknown;
      }
      if (type) {
        if (paddr = strtok(NULL, sep)) {
          addr = strtoul(paddr, NULL, 0); // Needs error checking added!
          // Get data for Poke, Set, Clear & Toggle
          if (type != c_peek) {
            if (pdata = strtok(NULL, sep)) {
              data = strtoul(pdata, NULL, 0); // Needs error checking added!
            } else {
              err = 1;
            }
          }
          if (!err) {
            if (!(paddr = strtok(NULL, sep))) {
              fd = open("/dev/mem", O_RDWR);
              if (fd > 0) {
                page_addr = (addr & ~(page_size - 1));
                page_offset = addr - page_addr;
                ptr = mmap(NULL, page_size, PROT_READ|PROT_WRITE, MAP_SHARED, fd, page_addr);
                if (ptr != MAP_FAILED) {

                  // Read existing data if Set, Clear or Toggle
                  if (type >= c_set) {
                    val = *((volatile unsigned int*)(ptr + page_offset));
                  }

                  // Set updated data
                  switch (type) {
                    case c_peek:
                      break;
                    case c_poke:
                      val = data;
                      break;
                    case c_set:
                      val = val | data;
                      break;
                    case c_clear:
                      val = val & ~data;
                      break;
                    case c_toggle:
                      val = val ^ data;
                      break;
                    default:
                      break;
                  }

                  // Write updated data & read it back if Poke, Set, Clear or Toggle
                  if (type != c_peek) {
                    *((volatile unsigned int*)(ptr + page_offset)) = val;
                  }

                  // Read data, existing for Peek, updated for Poke, Set, Clear and Toggle
                  val = *((volatile unsigned int*)(ptr + page_offset));

                  // Pass read data back to user
                  printf("0x%08X", val);

                } else printf("Error: Failed to mmap");
              } else printf("Error: Failed to open /dev/mem");
            } else printf("Error: Unexpected field found");
          } else printf("Error: Data field not found");
        } else printf("Error: Address field not found");
      } else printf("Error: Command field not recognised");
    } else printf("Error: Command field not found");
  } else printf("Error: No QUERY_STRING");
}

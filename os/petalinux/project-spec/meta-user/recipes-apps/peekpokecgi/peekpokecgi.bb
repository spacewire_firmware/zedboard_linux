#
# This is the peekpokecgi aplication recipe
#
#

SUMMARY = "peekpokecgi application"
SECTION = "PETALINUX/apps"
LICENSE = "MIT"
LIC_FILES_CHKSUM = "file://${COMMON_LICENSE_DIR}/MIT;md5=0835ade698e0bcf8506ecda2f7b4f302"
SRC_URI = "file://peek.c \
           file://poke.c \
           file://peekstring.c \
           file://loadfirmware.c \
           file://bitbash.c \
           file://Makefile \
          "
FILES_${PN} += "/srv/www/cgi-bin"
S = "${WORKDIR}"
CFLAGS_prepend = "-I ${S}/include"
do_compile() {
        oe_runmake
}
do_install() {
        install -d ${D}/srv/www/cgi-bin
        install -m 0755 ${S}/peek ${D}/srv/www/cgi-bin
        install -m 0755 ${S}/poke ${D}/srv/www/cgi-bin
        install -m 0755 ${S}/peekstring ${D}/srv/www/cgi-bin
        install -m 0755 ${S}/loadfirmware ${D}/srv/www/cgi-bin
        install -m 0755 ${S}/bitbash ${D}/srv/www/cgi-bin
}

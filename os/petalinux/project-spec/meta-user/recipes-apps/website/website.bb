#
# This file is the website recipe.
#

SUMMARY = "Simple website application"
SECTION = "PETALINUX/apps"
LICENSE = "MIT"
LIC_FILES_CHKSUM = "file://${COMMON_LICENSE_DIR}/MIT;md5=0835ade698e0bcf8506ecda2f7b4f302"

SRC_URI = "file://project.txt"
SRC_URI += "file://index.php"

SRC_URI += "file://share/header.php"
SRC_URI += "file://share/footer.php"
SRC_URI += "file://share/style.css"
SRC_URI += "file://share/script.js"
SRC_URI += "file://share/amber.gif"
SRC_URI += "file://share/green.gif"
SRC_URI += "file://share/red.gif"

SRC_URI += "file://cgi-bin/uptime.cgi"
SRC_URI += "file://cgi-bin/test-cgi"
SRC_URI += "file://cgi-bin/hello_world.php"
SRC_URI += "file://cgi-bin/sqlite_test.php"
SRC_URI += "file://cgi-bin/phpliteadmin.php"
SRC_URI += "file://cgi-bin/phpliteadmin.config.php"

SRC_URI += "file://home/index.php"

SRC_URI += "file://system/index.php"
SRC_URI += "file://system/script.js"

SRC_URI += "file://peekpoke/index.php"
SRC_URI += "file://peekpoke/script.js"

SRC_URI += "file://zedboard/index.php"
SRC_URI += "file://zedboard/script.js"
SRC_URI += "file://zedboard/style.css"
SRC_URI += "file://zedboard/zedboard.png"
SRC_URI += "file://zedboard/sw_on.png"
SRC_URI += "file://zedboard/sw_off.png"
SRC_URI += "file://zedboard/sw_none.png"
SRC_URI += "file://zedboard/btn_on.png"
SRC_URI += "file://zedboard/btn_off.png"
SRC_URI += "file://zedboard/btn_none.png"
SRC_URI += "file://zedboard/led_on.png"
SRC_URI += "file://zedboard/led_off.png"

FILES_${PN} += "/srv/www"

S = "${WORKDIR}"

do_install() {
  install -d ${D}/srv/www
  install -m 0644 ${S}/project.txt ${D}/srv/www
  install -m 0644 ${S}/index.php ${D}/srv/www

  install -d ${D}/srv/www/share
  install -m 0644 ${S}/share/header.php ${D}/srv/www/share
  install -m 0644 ${S}/share/footer.php ${D}/srv/www/share
  install -m 0644 ${S}/share/style.css ${D}/srv/www/share
  install -m 0644 ${S}/share/script.js ${D}/srv/www/share
  install -m 0644 ${S}/share/amber.gif ${D}/srv/www/share
  install -m 0644 ${S}/share/green.gif ${D}/srv/www/share
  install -m 0644 ${S}/share/red.gif ${D}/srv/www/share

  install -d ${D}/srv/www/cgi-bin
  install -m 0777 -d ${D}/srv/www/cgi-bin/db
  install -m 0755 ${S}/cgi-bin/uptime.cgi ${D}/srv/www/cgi-bin
  install -m 0755 ${S}/cgi-bin/test-cgi ${D}/srv/www/cgi-bin
  install -m 0644 ${S}/cgi-bin/hello_world.php ${D}/srv/www/cgi-bin
  install -m 0644 ${S}/cgi-bin/sqlite_test.php ${D}/srv/www/cgi-bin
  install -m 0644 ${S}/cgi-bin/phpliteadmin.php ${D}/srv/www/cgi-bin
  install -m 0644 ${S}/cgi-bin/phpliteadmin.config.php ${D}/srv/www/cgi-bin

  install -d ${D}/srv/www/home
  install -m 0644 ${S}/home/index.php ${D}/srv/www/home

  install -d ${D}/srv/www/system
  install -m 0644 ${S}/system/index.php ${D}/srv/www/system
  install -m 0644 ${S}/system/script.js ${D}/srv/www/system

  install -d ${D}/srv/www/peekpoke
  install -m 0644 ${S}/peekpoke/index.php ${D}/srv/www/peekpoke
  install -m 0644 ${S}/peekpoke/script.js ${D}/srv/www/peekpoke

  install -d ${D}/srv/www/zedboard
  install -m 0644 ${S}/zedboard/index.php ${D}/srv/www/zedboard
  install -m 0644 ${S}/zedboard/style.css ${D}/srv/www/zedboard
  install -m 0644 ${S}/zedboard/script.js ${D}/srv/www/zedboard
  install -m 0644 ${S}/zedboard/zedboard.png ${D}/srv/www/zedboard
  install -m 0644 ${S}/zedboard/sw_on.png ${D}/srv/www/zedboard
  install -m 0644 ${S}/zedboard/sw_off.png ${D}/srv/www/zedboard
  install -m 0644 ${S}/zedboard/sw_none.png ${D}/srv/www/zedboard
  install -m 0644 ${S}/zedboard/btn_on.png ${D}/srv/www/zedboard
  install -m 0644 ${S}/zedboard/btn_off.png ${D}/srv/www/zedboard
  install -m 0644 ${S}/zedboard/btn_none.png ${D}/srv/www/zedboard
  install -m 0644 ${S}/zedboard/led_on.png ${D}/srv/www/zedboard
  install -m 0644 ${S}/zedboard/led_off.png ${D}/srv/www/zedboard
}

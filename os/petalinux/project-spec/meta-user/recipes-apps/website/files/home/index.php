<?php
//
// File .......... index.php
// Author ........ Steve Haywood
// Website ....... http://www.spacewire.co.uk
// Project ....... SpaceWire UK Tutorial
// Version ....... 1.0
// Conception .... 27 February 2024
// Standard ...... PHP 7
// Description ...
//   Webpage for Home.
//
?>

<?php require '../share/header.php'; ?>

<div class="content" style="text-align: left;">
<h3>Welcome</h3>
<ul><li>
Hello and welcome to the Zedboard Webserver tutorial project provided by the guys over at SpaceWire UK.
</li></ul>
<h3>Links</h3>
<ul><li>
<a href="https://www.spacewire.co.uk/tutorial/index">Using Xilinx PetaLinux, Vitis &amp; Vivado 2021.2 with Xubuntu 20.04.3 on a Zedboard rev. D</a>
</li></ul>
</div>

<?php require '../share/footer.php'; ?>

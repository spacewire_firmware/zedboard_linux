<!--
File .......... index.php
Author ........ Steve Haywood
Website ....... http://www.spacewire.co.uk
Project ....... SpaceWire UK Tutorial
Version ....... 1.2
Conception .... 8 August 2023
Standard ...... PHP 7
Description ...
  Webpage for System Information & Firmware Load.
-->

<?php
// Get information
$sys_host = exec('hostname', $retval);
$sys_time = exec('date', $retval);
$sys_load = exec('awk \'{print $1}\' /proc/loadavg', $retval);
$sys_up = exec('awk \'{print $1}\' /proc/uptime', $retval);
$cpu_model = exec('grep model /proc/cpuinfo | cut -d : -f2 | tail -1 | sed \'s/\s//\'', $retval);
$cpu_cores = exec('grep -c ^processor /proc/cpuinfo', $retval);
$mem_total = exec('free -m | awk \'NR==2{print $2}\'', $retval);
$mem_used = exec('free -m | awk \'NR==2{print $3}\'', $retval);
$mem_free = exec('free -m | awk \'NR==2{print $4}\'', $retval);
$net_mac = exec('cat /sys/class/net/eth0/address', $retval);
$net_ip_loc = exec('ip a | grep inet | grep -vw lo | grep -v inet6 | cut -d \/ -f1 | sed \'s/[^0-9\.]*//g\'', $retval);
$net_ip_ext = exec('wget -q -O- http://ipecho.net/plain', $retval);
?>

<?php require '../share/header.php'; ?>

<div class="content">

<div class="inner">
<?php
$id = array(0 => "o", 1 => "");
$title = array(0 => "Operating System", 1 => "Firmware");
$button = array(0 => "read_os", 1 => "read");
for ($i = 0 ; $i <= 1 ; $i++) {
?>
<table>
<thead>
<tr><th colspan="3"><?php echo $title[$i] ?> Information <button onclick="<?php echo $button[$i] ?>_ids()">Read ID</button></th>
</tr>
</thead>
<tbody>
<tr>
<td style="text-align:right">Description :</td>
<td style="text-align:left" id="<?php echo $id[$i] ?>id_0">Unknown</td>
<td><img id="<?php echo $id[$i] ?>sid_0" style="vertical-align:middle" src="../share/amber.gif" title="Unknown!" alt="Missing Image!"></td>
</tr>
<tr>
<td style="text-align:right">Company :</td>
<td style="text-align:left" id="<?php echo $id[$i] ?>id_1">Unknown</td>
<td><img id="<?php echo $id[$i] ?>sid_1" style="vertical-align:middle" src="../share/amber.gif" title="Unknown!" alt="Missing Image!"></td>
</tr>
<tr>
<td style="text-align:right">Author :</td>
<td style="text-align:left" id="<?php echo $id[$i] ?>id_2">Unknown</td>
<td><img id="<?php echo $id[$i] ?>sid_2" style="vertical-align:middle" src="../share/amber.gif" title="Unknown!" alt="Missing Image!"></td>
</tr>
<tr>
<td style="text-align:right">Version :</td>
<td style="text-align:left" id="<?php echo $id[$i] ?>id_3">Unknown</td>
<td><img id="<?php echo $id[$i] ?>sid_3" style="vertical-align:middle" src="../share/amber.gif" title="Unknown!" alt="Missing Image!"></td>
</tr>
<tr>
<td style="text-align:right">Timestamp :</td>
<td style="text-align:left" id="<?php echo $id[$i] ?>id_4">Unknown</td>
<td><img id="<?php echo $id[$i] ?>sid_4" style="vertical-align:middle" src="../share/amber.gif" title="Unknown!" alt="Missing Image!"></td>
</tr>
<tr>
<td style="text-align:right">Hash :</td>
<td style="text-align:left" id="<?php echo $id[$i] ?>id_5">Unknown</td>
<td><img id="<?php echo $id[$i] ?>sid_5" style="vertical-align:middle" src="../share/amber.gif" title="Unknown!" alt="Missing Image!"></td>
</tr>
</tbody>
</table>
<?php
}
?>
</div>


<div class="inner">
<table>
<thead>
<tr><th colspan="3">Loadable Firmware</th></tr>
</thead>
<tbody>
<?php
  $dirPath = "/media/sd-mmcblk0p1/firmware";
  $files = scandir($dirPath);
  foreach ($files as $file) {
    $filePath = $dirPath . '/' . $file;
    if (is_file($filePath)) {
      echo "<tr><td style=\"text-align:left\">";
      echo $file;
      echo "</td><td><button onclick=\"loadfirmware('$file')\">Load</button></td><td><img id=\"fm_$file\" style=\"vertical-align:middle\" src=\"../share/amber.gif\" title=\"Unknown!\" alt=\"Missing Image!\"></td></tr>";
    }
  }
?>
</tbody>
</table>
</div>

<div class="inner">

<table>
<tr><th colspan="2">System</th></tr>
<tr><td>Hostname</td>
<td><?php echo $sys_host ?></td>
</tr><tr><td>Time</td><td><?php echo $sys_time ?></td></tr>
<tr><td>Uptime</td><td><span id="uptime_text"><?php echo $sys_up ?></span> seconds <button onclick="get_uptime()">Refresh</button> Auto :
<select id="uptime" onchange="uptime();">
  <option value="0">Off</option>
  <option value="1">1s</option>
  <option value="5">5s</option>
</select>
</td></tr>
</table>

<table>
<tr><th colspan="2">CPU</th></tr>
<tr><td>Model</td>
<td><?php echo $cpu_model ?></td></tr>
<tr><td>Cores</td><td><?php echo $cpu_cores ?></td></tr>
<tr><td>Load</td><td><?php echo $sys_load ?></td></tr>
</table>

<table>
<tr><th colspan="2">Memory</th></tr>
<tr><td>Total</td><td><?php echo $mem_total ?> Mb</td></tr>
<tr><td>Used</td><td><?php echo $mem_used ?> Mb</td></tr>
<tr><td>Free</td><td><?php echo $mem_free ?> Mb</td></tr>
</table>

<table>
<tr><th colspan="2">Network</th></tr>
<tr><td>MAC Address</td><td><?php echo $net_mac ?></td></tr>
<tr><td>Internal IP</td><td><?php echo $net_ip_loc ?></td></tr>
<tr><td>External IP</td><td><?php echo $net_ip_ext ?></td></tr>
</table>

</div>

</div>

<?php require '../share/footer.php'; ?>

//
// File .......... script.js
// Author ........ Steve Haywood
// Website ....... http://www.spacewire.co.uk
// Project ....... SpaceWire UK Tutorial
// Version ....... 1.7
// Conception .... 17 January 2022
// Standard ...... ECMA-262
// Description ...
//   Javascript for System Information & Firmware Load.
//

// Interval between Load Firmware & Read Firmware ID's
var timer_fw;

// Interval between PetaLinux uptime reads
var timer_uptime;

// Load PL Firmware
function loadfirmware(filename) {
  var url = "/cgi-bin/loadfirmware?" + filename;
  if (window.XMLHttpRequest) {
    var ajaxReq = new XMLHttpRequest();
    ajaxReq.onreadystatechange = function() {
      if (ajaxReq.readyState == 4 && ajaxReq.status == 200) {
        var respText = ajaxReq.responseText;
        var img_obj = document.getElementById("fm_" + filename);
        // Unique number is added to image to avoid caching issues on separate animations
        const now = Date.now();
        if (respText.substr(0,6) == "Error:") {
          img_obj.src = "../share/red.gif?" + now;
          img_obj.title = "Last loadfirmware failed : " + respText.substr(7);
        } else {
          img_obj.src = "../share/green.gif?" + now;
          img_obj.title = "Last loadfirmware successful";
        }
        clearInterval(timer_fw);
        timer_fw = setInterval(read_ids(), 1000);
      }
    }
    ajaxReq.open("POST", url, true);
    ajaxReq.send(null);
  }
}

// Download OS information file & display result
async function read_os_ids() {
  let response = await fetch("/project.txt?" + Date.now());
  if (response.status == 200) {
    let ids = await response.text();
    fields = ids.split(/\r?\n/);
  }
  for (var i = 0; i < 6; i++) {
    const now = Date.now();
    const txt_obj = document.getElementById("oid_" + i);
    const img_obj = document.getElementById("osid_" + i);
    if (response.status == 200) {
      if (i < fields.length && fields[i] != "") {
        img_obj.src = "../share/green.gif?" + now;
        img_obj.title = "Last file fetch successful";
        txt_obj.innerHTML = fields[i];
      } else {
        img_obj.src = "../share/red.gif?" + now;
        img_obj.title = "Missing field information";
        txt_obj.innerHTML = "Unknown";
      }
    } else {
      img_obj.src = "../share/red.gif?" + now;
      img_obj.title = "Last file fetch failed";
      txt_obj.innerHTML = "Unknown";
    }
  }
}

// Peek all strings
function read_ids() {
  read_id(c_id_description, 0);
  read_id(c_id_company, 1);
  read_id(c_id_author, 2);
  read_id(c_id_version, 3);
  read_id(c_id_timestamp, 4);
  read_id(c_id_hash, 5);
}

// Peek string & display result
function read_id(offset, reg) {
  var url = "/cgi-bin/peekstring?" + c_axi_identification + "&" + 4096 + "&" + offset + "&" + 128;
  if (window.XMLHttpRequest) {
    var ajaxReq = new XMLHttpRequest();
    ajaxReq.onreadystatechange = function() {
      if (ajaxReq.readyState == 4 && ajaxReq.status == 200) {
        var respText = ajaxReq.responseText;
        var img_obj = document.getElementById("sid_" + reg);
        // Unique number is added to image to avoid caching issues on separate animations
        const now = Date.now();
        if (respText.substr(0,6) == "Error:") {
          img_obj.src = "../share/red.gif?" + now;
          img_obj.title = "Last peekstring failed : " + respText.substr(7);
        } else {
          const now = Date.now();
          img_obj.src = "../share/green.gif?" + now;
          img_obj.title = "Last peekstring successful";
          document.getElementById("id_" + reg).innerHTML = respText;
        }
      }
    }
    ajaxReq.open("POST", url, true);
    ajaxReq.send(null);
  }
}

// Get uptime
function get_uptime() {
  var url = "/cgi-bin/uptime.cgi";
  if (window.XMLHttpRequest) {
    var ajaxReq = new XMLHttpRequest();
    ajaxReq.onreadystatechange = function() {
      if (ajaxReq.readyState == 4 && ajaxReq.status == 200) {
        var respText = ajaxReq.responseText;
        var txtObj = document.getElementById("uptime_text");
        if (txtObj) {
          txtObj.innerHTML = respText;
        }
      }
    }
    ajaxReq.open("POST", url, true);
    ajaxReq.send(null);
  }
}

// Update uptime timer
function uptime() {
  clearInterval(timer_uptime);
  var uptime = document.getElementById("uptime");
  if (uptime) {
    var interval = uptime.value;
    if (interval > 0) {
      timer_uptime = setInterval("get_uptime()", 1000 * interval);
    }
  }
}

<?php
//
// File .......... footer.php
// Author ........ Steve Haywood
// Website ....... http://www.spacewire.co.uk
// Project ....... SpaceWire UK Tutorial
// Version ....... 1.0
// Conception .... 27 February 2024
// Standard ...... PHP 7
// Description ...
//   Footer include for website pages.
//
?>

<div class="section">Designed by Steve Haywood @ 2021-<?php echo date("Y") ?></div>
<?php
// Add global JS if one exists
if (file_exists("../share/script.js")) {
    echo '<script src="../share/script.js"></script>';
}
// Add local JS if one exists
if (file_exists("script.js")) {
    echo '<script src="script.js"></script>';
}
?>
</body>
</html>

<?php
//
// File .......... header.php
// Author ........ Steve Haywood
// Website ....... http://www.spacewire.co.uk
// Project ....... SpaceWire UK Tutorial
// Version ....... 1.0
// Conception .... 27 February 2024
// Standard ...... PHP 7
// Description ...
//   Header include for website pages.
//
?>

<?php

// Menu Key = Subdirectory Name
const c_tab      = 0; // Menu Name
const c_title    = 1; // Page Tile & Header Text
const c_onload   = 2; // OnLoad Calls
const c_submenu  = 3; // Submenu Array
// Sub-Menu Key = Webpage
const c_sub_tab  = 0; // Menu Name

// Define pages
$pages = array(
  "home" => array (
    "Home",
    "Home",
    "",
    array ()
    ),
  "system" => array (
    "System",
    "System Information &amp; Firmware Load",
    "read_os_ids(); read_ids()",
    array ()
    ),
  "peekpoke" => array (
    "Peek &amp; Poke",
    "Peek &amp; Poke Addresses",
    "add_register()",
    array ()
    ),
  "zedboard" => array (
    "Zedboard",
    "Interactive Zedboard",
    "",
    array ()
    ),
  "misc" => array (
    "Misc",
    "Miscellaneous",
    "",
    array (
           "test-cgi" => array("CGI Basic Test"),
           "hello_world.php" => array("PHP Basic Test"),
           "sqlite_test.php" => array ("SQLite Basic Test"),
           "phpliteadmin.php" => array("PHP Lite Admin"),
          ),
    )
);

// Get subdirectory webpage is being served from
$base = basename(dirname($_SERVER['PHP_SELF']));
?>

<!DOCTYPE html>
<html lang="en">
<head>
<meta http-equiv="content-type" content="text/html; charset=UTF-8">
<?php
// Add global CSS if one exists
if (file_exists("../share/style.css")) {
    echo '<link href="../share/style.css" rel="stylesheet">';
}
// Add local CSS if one exists
if (file_exists("style.css")) {
    echo '<link href="style.css" rel="stylesheet">';
}
?>
<title><?php echo $pages[$base][c_title] ?></title>
</head>
<body onload="<?php echo $pages[$base][c_onload] ?>">
<div class="section"><h2><?php echo $pages[$base][c_title] ?></h2></div>

<div class="menu">
<ul class="menu_ul">
<?php
foreach ($pages as $key => $fields) {
  if ($key == $base) {
    $colour = "000";
  } else {
    $colour = "666";
  }
  if (count($fields[c_submenu]) > 0) {
    echo '<li class="menu_li"><a style="color:#'.$colour.'" href="#">'.$fields[c_tab].'</a>';
    echo '<ul class="menu_ul">';
    foreach ($fields[c_submenu] as $subkey => $subfields) {
      echo '<li class="menu_li"><a href="/cgi-bin/'.$subkey.'" target=_blank>'.$subfields[c_sub_tab].'</a></li>';
    }
    echo '</ul>';
    echo '</li>';
  } else {
    echo '<li class="menu_li"><a style="color:#'.$colour.'" href="../'.$key.'">'.$fields[c_tab].'</a></li>';
  }
}
?>
</ul>
</div>

//
// File .......... script.js
// Author ........ Steve Haywood
// Website ....... http://www.spacewire.co.uk
// Project ....... SpaceWire UK Tutorial
// Version ....... 1.7
// Conception .... 28 February 2024
// Standard ...... ECMA-262
// Description ...
//   Global Javascript for use in all webpages.
//

// Constants
const c_axi_identification = 0x40000000;
const c_id_description     = 0x0000;
const c_id_company         = 0x0080;
const c_id_author          = 0x00C0;
const c_id_version         = 0x0100;
const c_id_timestamp       = 0x0120;
const c_id_hash            = 0x0140;

const c_axi_gpio_zed       = 0x40010000;
const c_gpio_leds          = 0x0000;
const c_gpio_switches      = 0x0008;
const c_gpio_buttons       = 0x0010;

// Null function
function nullfunc(respText) {
}

//
// Javascript wrapper for call to bitbash CGI
// cmd .... "peek", "poke", "set", "clear", "toggle"
// addr ... 0x40010000 (for example, address to access)
// data ... 0x000000FF (for example, data to write or mask to apply, not used for peek)
// func ... Javascript function to call upto success
//
function bitbash(cmd, addr, data, func) {
  var url = "/cgi-bin/bitbash?" + cmd + "&" + addr;
  if (cmd != "peek") {
    url = url + "&" + data;
  }
  if (window.XMLHttpRequest) {
    var ajaxReq = new XMLHttpRequest();
    ajaxReq.onreadystatechange = function() {
      if (ajaxReq.readyState == 4 && ajaxReq.status == 200) {
        var respText = ajaxReq.responseText;
        func(respText);
      }
    }
    ajaxReq.open("POST", url, true);
    ajaxReq.send(null);
  }
}

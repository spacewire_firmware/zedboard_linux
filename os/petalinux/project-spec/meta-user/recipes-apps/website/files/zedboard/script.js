//
// File .......... script.js
// Author ........ Steve Haywood
// Website ....... http://www.spacewire.co.uk
// Project ....... SpaceWire UK Tutorial
// Version ....... 1.7
// Conception .... 28 February 2024
// Standard ...... ECMA-262
// Description ...
//   Javascript for Interactive Zedboard.
//

// Interval between status reads
let timer_status;

// Read LEDs, switches & buttons registers and update image overlays
function get_status() {
  bitbash("peek", c_axi_gpio_zed + c_gpio_leds, 0, update_leds);
  bitbash("peek", c_axi_gpio_zed + c_gpio_switches, 0, update_switches);
  bitbash("peek", c_axi_gpio_zed + c_gpio_buttons, 0, update_buttons);
}

// Update status timer
function status() {
  clearInterval(timer_status);
  var status = document.getElementById("status");
  if (status) {
    var interval = status.value;
    if (interval > 0) {
      timer_status = setInterval("get_status()", 100 * interval);
    }
  }
}

// Update Zedboard LEDs image overlays
function update_leds(respText) {
  for (var i = 0; i < 8; i++) {
    var img_obj = document.getElementById("id_led_" + i);
    if (img_obj) {
      const now = Date.now();
      const value = parseInt(respText);
      if (value & (2**i)) {
        img_obj.src = "led_on.png?" + now;
      } else {
        img_obj.src = "led_off.png?" + now;
      }
    }
  }
}

// Update Zedboard switches overlays
function update_switches(respText) {
  for (var i = 0; i < 8; i++) {
    var img_obj = document.getElementById("id_sw_" + i);
    if (img_obj) {
      const now = Date.now();
      const value = parseInt(respText);
      if (value & (2**i)) {
        img_obj.src = "sw_on.png?" + now;
      } else {
        img_obj.src = "sw_off.png?" + now;
      }
    }
  }
}

// Update Zedboard buttons image overlays
function update_buttons(respText) {
  for (var i = 0; i < 5; i++) {
    var img_obj = document.getElementById("id_btn_" + i);
    const value = parseInt(respText);
    if (img_obj) {
      const now = Date.now();
      if (value & (2**i)) {
        img_obj.src = "btn_on.png?" + now;
      } else {
        img_obj.src = "btn_off.png?" + now;
      }
    }
  }
}

// Toggle LED on/off
function toggle_led(mask) {
  bitbash('toggle', c_axi_gpio_zed + c_gpio_leds, mask, nullfunc);
}

// Clear button state
function clear_button(mask) {
  bitbash('clear', c_axi_gpio_zed + c_gpio_buttons, mask, nullfunc);
}

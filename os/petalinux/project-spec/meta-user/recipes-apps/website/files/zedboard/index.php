<?php
/*
 *
 * File .......... index.php
 * Author ........ Steve Haywood
 * Website ....... http://www.spacewire.co.uk
 * Project ....... SpaceWire UK Tutorial
 * Version ....... 1.0
 * Conception .... 26 February 2024
 * Standard ...... PHP 7
 * Description ...
 *   Webpage for Interactive Zedboard.
 *
 */
?>

<?php require '../share/header.php'; ?>

<div class="content" style="text-align:left">

<h3>Interactive Zedboard :-</h3>
<ul>
<li><button onclick="get_status()">Read</button> board state for LEDs, switches &amp; push buttons.</li>
<li>Automatically refresh board state every <select id="status" onchange="status();">
  <option value="0">Never</option>
  <option value="1">100</option>
  <option value="10">1000</option>
</select> ms.</li>
<li>Toggle LEDs on/off by clicking on them.</li>
<li>Clear latched push button states by clicking on them.</li>
</ul>

<div class="zedboard">

<img src="zedboard.png" alt="Missing Image!" usemap="#zedmap">

<img id="id_led_0" class="led_0" src="led_off.png" alt="">
<img id="id_led_1" class="led_1" src="led_off.png" alt="">
<img id="id_led_2" class="led_2" src="led_off.png" alt="">
<img id="id_led_3" class="led_3" src="led_off.png" alt="">
<img id="id_led_4" class="led_4" src="led_off.png" alt="">
<img id="id_led_5" class="led_5" src="led_off.png" alt="">
<img id="id_led_6" class="led_6" src="led_off.png" alt="">
<img id="id_led_7" class="led_7" src="led_off.png" alt="">

<img id="id_sw_0" class="sw_0" src="sw_none.png" alt="">
<img id="id_sw_1" class="sw_1" src="sw_none.png" alt="">
<img id="id_sw_2" class="sw_2" src="sw_none.png" alt="">
<img id="id_sw_3" class="sw_3" src="sw_none.png" alt="">
<img id="id_sw_4" class="sw_4" src="sw_none.png" alt="">
<img id="id_sw_5" class="sw_5" src="sw_none.png" alt="">
<img id="id_sw_6" class="sw_6" src="sw_none.png" alt="">
<img id="id_sw_7" class="sw_7" src="sw_none.png" alt="">

<img id="id_btn_0" class="btn_0" src="btn_none.png" alt="">
<img id="id_btn_1" class="btn_1" src="btn_none.png" alt="">
<img id="id_btn_2" class="btn_2" src="btn_none.png" alt="">
<img id="id_btn_3" class="btn_3" src="btn_none.png" alt="">
<img id="id_btn_4" class="btn_4" src="btn_none.png" alt="">

</div>

<map id="zedmap" name="zedmap">

<area title="Toggle LED 0 On/Off" shape="rect" coords="382,443,405,461" alt="Missing Image!" href="#" onclick="toggle_led(1)">
<area title="Toggle LED 1 On/Off" shape="rect" coords="356,443,379,461" alt="Missing Image!" href="#" onclick="toggle_led(2)">
<area title="Toggle LED 2 On/Off" shape="rect" coords="330,443,353,461" alt="Missing Image!" href="#" onclick="toggle_led(4)">
<area title="Toggle LED 3 On/Off" shape="rect" coords="304,443,327,461" alt="Missing Image!" href="#" onclick="toggle_led(8)">
<area title="Toggle LED 4 On/Off" shape="rect" coords="278,443,301,461" alt="Missing Image!" href="#" onclick="toggle_led(16)">
<area title="Toggle LED 5 On/Off" shape="rect" coords="252,443,275,461" alt="Missing Image!" href="#" onclick="toggle_led(32)">
<area title="Toggle LED 6 On/Off" shape="rect" coords="226,443,249,461" alt="Missing Image!" href="#" onclick="toggle_led(64)">
<area title="Toggle LED 7 On/Off" shape="rect" coords="200,443,223,461" alt="Missing Image!" href="#" onclick="toggle_led(128)">

<area title="Clear top button state" shape="rect" coords="486,416,519,441" alt="Missing Image!" href="#" onclick="clear_button(16)">
<area title="Clear centre button state" shape="rect" coords="486,446,519,471" alt="Missing Image!" href="#" onclick="clear_button(1)">
<area title="Clear bottom button state" shape="rect" coords="486,477,519,502" alt="Missing Image!" href="#" onclick="clear_button(2)">
<area title="Clear left button state" shape="rect" coords="337,446,480,471" alt="Missing Image!" href="#" onclick="clear_button(4)">
<area title="Clear right button state" shape="rect" coords="525,446,558,471" alt="Missing Image!" href="#" onclick="clear_button(8)">

</map>

</div>

<?php require '../share/footer.php'; ?>

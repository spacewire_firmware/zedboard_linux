<?php
//
// File .......... index.php
// Author ........ Steve Haywood
// Website ....... http://www.spacewire.co.uk
// Project ....... SpaceWire UK Tutorial
// Version ....... 1.0
// Conception .... 27 February 2024
// Standard ...... PHP 7
// Description ...
//   Forwarder page for use in root www directory.
//
?>

<?php
header("Location: home");
exit;
?>

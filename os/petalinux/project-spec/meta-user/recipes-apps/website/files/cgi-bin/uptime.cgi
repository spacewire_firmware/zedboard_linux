#!/bin/sh

# Output Header
printf "Content-type: text/html\r\n\r\n"

# Output Command
awk '{print $1}' /proc/uptime

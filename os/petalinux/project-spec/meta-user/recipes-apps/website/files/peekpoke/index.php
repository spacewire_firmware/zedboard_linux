<?php
//
// File .......... index.php
// Author ........ Steve Haywood
// Website ....... http://www.spacewire.co.uk
// Project ....... SpaceWire UK Tutorial
// Version ....... 1.2
// Conception .... 8 August 2023
// Standard ...... PHP 7
// Description ...
//   Webpage for Peek & Poke Addresses.
//
?>

<?php require '../share/header.php'; ?>

<div class="content">

<table id="registers">
<tr>
<th>Address</th>
<th>Hex</th>
<th>Peek Value</th>
<th>Sel</th>
<th>Peek</th>
<th>Status</th>
<th>Copy</th>
<th colspan="2">Poke Value</th>
<th>Sel</th>
<th>Poke</th>
<th>Status</th>
<th>Description</th>
</tr>
</table>
<br><br>
<input title="Add new row to end of address table" type="button" value="Add" onclick="add_row()">
<select title="Set type of row to add to address table" id="type">
  <option value="0">Register</option>
  <option value="1">Section</option>
</select>
<input title="Remove last address from table" type="button" value="Remove" onclick="rem_register()">
<input title="Peek all selected addresses in table" type="button" value="Peek All" onclick="peek_all()">
<input title="Copy all table peek values into poke values" type="button" value="Copy All" onclick="copy_all()">
<input title="Poke all selected addresses in table" type="button" value="Poke All" onclick="poke_all()">
Peek Refresh :
<select title="Set timer interval for automatic peek of table addresses" id="timer" onchange="timer()">
  <option value="0">Off</option>
  <option value="1">1s</option>
  <option value="5">5s</option>
</select>
Configuration :
<button title="Create configuration file from table" onclick="create_config()">Create...</button> <a title="Right click and Save Link As... to locate and rename this file" download="config.txt" id="download" href="" style="display: none">config.txt</a>
<input title="Read configuration file into table" type="file" id="load_config">

</div>

<?php require '../share/footer.php'; ?>

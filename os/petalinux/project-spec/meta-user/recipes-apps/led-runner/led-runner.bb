#
# This file is the led-runner recipe.
#
SUMMARY = "Simple led-runner application"
SECTION = "PETALINUX/apps"
LICENSE = "MIT"
LIC_FILES_CHKSUM ="file://${COMMON_LICENSE_DIR}/MIT;md5=0835ade698e0bcf8506ecda2f7b4f302"

SRC_URI = "file://led-runner"

S = "${WORKDIR}"

FILESEXTRAPATHS_prepend := "${THISDIR}/files:"

inherit update-rc.d

INITSCRIPT_NAME = "led-runner"
INITSCRIPT_PARAMS = "start 99 S ."

do_install() {
  install -d ${D}${sysconfdir}/init.d
  install -m 0755 ${S}/led-runner ${D}${sysconfdir}/init.d/led-runner
}
FILES_${PN} += "${sysconfdir}/*"

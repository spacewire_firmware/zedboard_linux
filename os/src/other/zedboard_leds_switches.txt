sec|AXI General Purpose IO - Zedboard Specific
range|0x40010000|false|true|0#0#255|true|LEDs
reg|0x40010008|true|true|0x00000000|false|DIP Switches
reg|0x40010010|true|true|0x00000000|false|Push Buttons
select|0x4001011C|true|true|0#0x00000000^Disabled#0x80000000^Enabled|true|Interrupt Enable
select|0x40010128|false|true|0#0^No Channel#1^Channel 1 (LEDs)#2^Channel 2 (DIP Switches)#4^Channel 3 (Push Buttons)|true|Interrupt Enable
select|0x40010120|false|true|0#0^No Channel#1^Channel 1 (LEDs)#2^Channel 2 (DIP Switches)#4^Channel 3 (Push Buttons)|true|Interrupt Status

sec|AXI Register Bank
reg|0x40020000|true|true|0x00000000|true|Register 0
reg|0x40020004|true|true|0x456789AB|true|Register 1
reg|0x40020008|true|true|0x00000000|true|Register 2
reg|0x4002000C|true|true|0x0000FF00|true|Register 3
